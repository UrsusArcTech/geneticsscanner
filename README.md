Automated extraction from Prokka annotated files

This does the following:

- When "extractor.R" is run, it looks in its directory for *.gbk files
- It will then search for the "aacA4" gene, find its complement orientation, along with the base locations of the gene. This will add "aacA4" to the description.
- It will then extract the complement or non-complement sequence, while also reverse complementing the complement sequences.
- Furthermore the upstream promoters are extracted from both sequence types.This will add "upstream promoter" in description.
- Then the script will find if "bla_" and "cat" genes, in that order, are adjacent to "aacA4", if so, adds "In37" to the description.
- Lastly, there is a whole file search for "yokD", "neo", and "ant1". If individually found, this adds "aac(3')-IId", "aph(3')", and "aadA1", respectively, to the description.

Instructions

- Put "extractor.R" into a folder that contains any .gbk files, run it, all output is put to "output.csv" in the same directory as the extractor.

Other uses

- For other projects, where extraction of nucleotides is required, you can extract and use the function: "extractSequence <- function(sequenceToSearch, start, end, originLinePos)". The "sequenceToSearch" is a vector that contains each line of a sequence extracted from the gbk, start and end are the inclusive nucleotide positions we want, originLinePos is the position in the vector where the ORIGIN is found (see Prokka annotation). This will return an inclusive nucleotide string.
- The "reverseDNAComplement <- function(sequence){" returns a reverse complement sequence.
